import { Pipe, PipeTransform } from '@angular/core';

/*
 * Author: sooms1, dec 2021: https://gitlab.com/sooms1
 * CryptocurrencyDecimalsPipe provides a more variable method of piping with decimals.
 * 
 * The regular Angular DecimalPipe (https://angular.io/api/common/DecimalPipe) did not fit my requirements
 * when I was developing a cryptocurrency price watcher.
 * 
 * e.g.: Rounding will not be the same for every case when working with a numbers very high, or really low at the same time.
 * For higher numbers I would have just liked to show two decimals: 57,384 => 57,38,
 * but that same pipe would cause values like 0.000014 to just become 0.00, which is not what I want obviously.
 * 
 * If you find yourself having trouble with this too, this Pipe might help.
 * 
 * This pipe provides a more flexible way of rounding decimals, while implementing is just as easy.
 * See the official documentation on Angular pipes here: https://angular.io/guide/pipes
 * 
 * More information on this particular can be found in this GitLab repo: https://gitlab.com/sooms1/cryptocurrencydecimalspipe
 */

@Pipe({
  name: 'cryptocurrencyDecimals'
})
export class CryptocurrencyDecimalsPipe implements PipeTransform {

  /*
   * The main method of this Pipe. 

   * It should be called with a value before the pipe and has the following optional parameters:
   * decimals: The preferred amount of decimals to return (default: 2)
   * showThousands: Add a comma separator for thousands, just like the DecimalPipe from Angular (default: true)
   * separator: When working with comma's as decimal points, change the separator (default: '.')
   * 
   * It returns the provided value, but slightly altered based on Pipe settings.
   */
  transform(value: any, decimals: number = 2, showThousands: boolean = true, separator: String = '.'): any {
    if (value === '') {
      return ''; 
    }

    // No usage of Math.floor or Math.trunc, because numbers will not be 100% identical anymore
    let integral, fractional;
    integral = value.split(separator)[0]; // The number before the decimal point in value
    fractional = value.split(separator)[1]; // The number after the decimal point in value

    fractional = this.transformFractional(fractional, decimals);

    if (showThousands) { // Optional parameter
      integral = this.transformIntegral(integral);
    } 
    return integral + (fractional ? `.${fractional}` : ''); // Return integral.fractional if fractional exists, otherwise just return integral
  }

  /*
   * Required method that should be called with fractional value and preferred amount of decimals when used.
   * It will format the decimals without any rounding to the preferred amount of decimals provided.
   * 
   * When decimals have a lot of prefixing zeroes, it will return longer fractionals.
   * 
   * e.g.: 00014 will be returned as 00014, but 98724 will be returned as 98 if decimals variable is 2.
   */
  transformFractional(fractional, decimals) {
    if (!fractional) {
      return String('').padStart(decimals, '0'); // Return string of zeroes with length equal to decimals variable
    }

    if (fractional.length < decimals) {
      return String(fractional).padEnd(decimals, '0'); // Return fractional with suffix zeroes with length equal to decimals variable
    }

    if (fractional.length === decimals) {
      return fractional; // Return fractional
    } 

    // Processing fractionals with length longer than decimals variable
    let newFractional = '';
    let regex = /^0*$/; // Regex for checking if newFractional only consists of zeroes
    for (let decimal in fractional) {
      newFractional += fractional[decimal];
      if (regex.test(newFractional)) { // Each time regex is true, add one more decimal to the provided decimals variable
        decimals += 1;
      }
      if (newFractional.length >= decimals) {
        return newFractional;
      }
    }
  }

  /*
   * Optional method that should be called with integral value when used.
   * It will replace the provided integral with a comma separated integral for showThousands.
   * e.g.: 45982 will be returned as 45,982.
   * 
   * If only this is needed, you're better off using the built-in DecimalPipe (https://angular.io/api/common/DecimalPipe).
   */
  transformIntegral(integral) {
    let regex = /(\d+)(\d{3})/; // Regex for dividing provided integral in sets of three numbers
    while (regex.test(integral)) {
      integral = integral.replace(regex, `${'$1'},${'$2'}`); // Replace integral with comma separated regex sets
    }

    return integral;
  }

}
